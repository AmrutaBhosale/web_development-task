**Version Control: **
Should be done on same repo 

We will be monitoring your commits and how you handle git totally 

**Growth:**

Use of Gem devise for login and logout. 
Mobile Friendly pages will be appreciated but not a compulsion

Ecommerce Site (with only PLP, PDP, Add To Cart, PlaceOrder, Order Page)
**User Flow:**
	PLP -> PDP -> Add To Cart Button -> Cart Page -> Place Order Button -> Order Page (Thank you page)

First Do this only with one product
After this do the task of more product to add to cart


> PLP (Product Listing Page):
This is the page where we see the number of products. Your goal is to make a page with at least 10 products on the listing page. This listing should be clickable and it should take you to PDP Page.
	
	_Things needed on the listing page:_
_Product Image 
Title
Price
Discounted Price_


For reference you can see out mirraw's listing page https://www.mirraw.com/store/lehengas


> PDP (Product Detail Page):
	This is the page where you enter once you click a product in the listing. Your goal is to make a page that will have all the information about the product you clicked. You have the full creativity on how this page should look. Main things are it should have Product image with slider function (nor necessary/important), Product Title, Discounted Price, Price.
Important: There should be a Add To Cart button.

_Cart Page:_
	This is the page where you will see products added to you cart once it is clicked from PDP page. Full creativity freedom on how it should look.
Important: There should be a Place Order button which will order your product in the cart.

_Thank you Page:_
	Once placed an order we need to show a thankyou page with the product ordered. Full Creativity freedom.
If you are using gem then this page should also show user details like name, email address and phone number..


**_Foundation :_**

1. Create a order listing page where all placed orders are visible along with their in/out (in means product is in warehouse, out means product has left the warehouse) status 
Single Page with pagination is a must here
2. Create Shipment Button (on same order listing page) should : 
Create an entry of the order in shipments table, assign an autogen invoice number and create an entry for the invoice details in the shipments table

Notes:
1. Basic DB structure will have 4 tables : users,orders,shipments,product with basic details about the product etc
2. Orders table should have a foreign key ref from Shipments table once a shipment is created for that order. (Basically a column having the shipment_id from shipments table for that particular order).
3. Can do 2 ^ or Just have a reference number for both. For example a column named `ref_number` in both orders and shipments table. 

**Deployment :**
Deploy the app on heroku along with db if possible, there are many videos for this.
